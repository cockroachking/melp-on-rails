# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
require 'csv'

# External CSV file to seed the database
restaurants_csv_path = Rails.root.join('db', 'seeds', 'restaurantes.csv')
# Iterate through each row of the CSV file and create a row in the 'restaurants'
# table with their respective columns
CSV.foreach(restaurants_csv_path, headers: true) do |row|
  r = Restaurant.create row.to_h
  puts "Restaurant '#{r.name}' has been saved"
end
