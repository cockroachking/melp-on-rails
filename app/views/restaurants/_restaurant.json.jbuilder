json.extract! restaurant, :id, :rating, :name, :site, :email, :phone, :street, :city, :state, :lat, :lng, :created_at, :updated_at
json.url restaurant_url(restaurant, format: :json)
