class RestaurantsController < ApplicationController
  include RestaurantsHelper
  # Skip Athenticity token to be able to test the API
  # by external resources
  skip_before_action :verify_authenticity_token
  before_action :set_restaurant, only: %i[show edit update destroy]

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.all
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show; end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
  end

  # GET /restaurants/1/edit
  def edit; end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(restaurant_params)

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: { errors: @restaurant.errors }, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: { errors: @restaurant.errors }, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant.destroy
    respond_to do |format|
      format.html { redirect_to restaurants_url, notice: 'Restaurant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def stadistics
    # An instance of the Boundary helper class
    # It will validate the constraints required for the parameters
    # taken directly from the ´params´ Hash
    boundary = Boundary.new params

    if boundary.valid?
      # The boundary circle has proper format

      # Making use of PostGIS extension the following query
      # returns a result set as follows:
      # {
      #   count: Count of restaurants that fall inside the circle with center [x,y] y radius z,
      #   avg: Average rating of restaurant inside the circle,
      #   std: Standard deviation of rating of restaurants inside the circle
      # }
      # Taking into account that ´radius´ must be specified in meters
      query = ActiveRecord::Base.sanitize_sql_array [
        %{
          SELECT COUNT(*), CAST(AVG(rating) AS FLOAT), CAST(STDDEV(rating) AS FLOAT) AS std
          FROM restaurants
          WHERE ST_DWithin(ST_SetSRID(ST_MakePoint(lng, lat), 4326), ST_MakePoint(?, ?)::geography, ?)
        },
        boundary.longitude,
        boundary.latitude,
        boundary.radius
      ]

      result = ActiveRecord::Base.connection.execute(query).first

      # The resulting JSON is rendered as response
      render json: result

    else
      # The parameters have no the appropiate format

      # A JSON with the description of the errors is rendered as response
      render json: { errors: boundary.errors }, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_restaurant
    @restaurant = Restaurant.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def restaurant_params
    params.require(:restaurant).permit(:id, :rating, :name, :site, :email, :phone, :street, :city, :state, :lat, :lng)
  end
end
