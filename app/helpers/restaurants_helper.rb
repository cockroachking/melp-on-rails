module RestaurantsHelper
  # Helper class to validate a circle coordinates
  # The parameters are taken directly from the `params` Hash
  # latitude: x
  # longitude: y
  # radius: z
  class Boundary
    include ActiveModel::Validations

    attr_accessor :latitude, :longitude, :radius

    validates :longitude, presence: true, numericality: {
      greater_than_or_equal_to: -180,
      less_than_or_equal_to: 180
    }
    validates :latitude, presence: true, numericality: {
      greater_than_or_equal_to: -90,
      less_than_or_equal_to: 90
    }
    validates :radius, presence: true, numericality: { greater_than_or_equal_to: 0 }

    def initialize(params)
      @longitude = params[:longitude]
      @latitude = params[:latitude]
      @radius = params[:radius]
    end
  end
end