class Restaurant < ApplicationRecord
  validates :id, uniqueness: true
  validates :rating, presence: true, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :lng, presence: true, numericality: {
    greater_than_or_equal_to: -180,
    less_than_or_equal_to: 180
  }
  validates :lat, presence: true, numericality: {
    greater_than_or_equal_to: -90,
    less_than_or_equal_to: 90
  }
end
